
	;deftemplate задают шаблоны в виде "структур данных", по которым создаются новые факты в базе фактов

	;candidate - Наш как бы фрейм с полями имя и возраст (по умолчанию),ъ; стаж работы в годах, основной язык программирования (не заданы, находим в процессе "собеседования")
	;в шаблонах есть 2-3 типа полей:
	;1) slot - одно слово целиком без пробелов либо несколько через разделитель (year.experience\\unknown)
	;2) multislot - последовательность из нескольких слов и знаков через пробелы (Jason Black, Margaret Thatcher, Donald J. Trump)
	;3) field - близок к slot

(deftemplate candidate					;основная структура, основной тип данных, заполняем поля в процессе интервиью
	(multislot Name)
	(multislot Age)
	(multislot Experience-years)
	(multislot Programming-language))

(deftemplate common									;вспомогательная структура с основными скиллами, которые отследживаются в процессе интервью
    (field oop (type SYMBOL)(default unknown))				;знает ли кандидат ООП
    (field fp (type SYMBOL)(default unknown))				;знает ли функц. программирование (не самое важное, если не знает вообще ничего ---для возможного обучения)
    (field experience-more-year (type SYMBOL)(default unknown))	;стаж в годах (<1, 1<.<2, >2)
    (field ready-to-study (type SYMBOL)(default unknown))	;готов ли к обучению (у кандтидата - 3 возм исхода: принят\до свидания\переобучение)
    (field database (type SYMBOL)(default unknown))			;знает ли бд
    (field working-os (type SYMBOL)(default unknown))		;основная операционка
    (field status (type SYMBOL)(default unknown)(allowed-symbols hired not.hired probationing unknown)))	;конечный статус - три возможных варианта: принят\не принят\на стажировку

(deftemplate rule
(multislot if)
(multislot then))

(defrule propagate-goal ""
    (goal is ?goal)
    (rule (if ?variable $?)
    (then ?goal ? ?value))
    =>
    (assert (goal is ?variable)))

(defrule goal-satified ""
    (declare (salience 30))
    ?f <- (goal is ?goal)
    (variable ?goal ?value)
    (answer ? ?text ?goal)
    =>
    (retract ?f)
    (format t "%s%s%n" ?text ?value))

(defrule remove-rule-no-match ""
    (declare (salience 20))
    (variable ?variable ?value)
    ?f <- (rule (if ?variable ? ~?value $?))
    =>
    (retract ?f))

(defrule modify-rule-match ""
    (declare (salience 20))
    (variable ?variable ?value)
    ?f <- (rule (if ?variable ? ?value and $?rest))
    =>
    (modify ?f (if ?rest)))

(defrule rule-satisfied ""
    (declare (salience 20))
    (variable ?variable ?value)
    ?f <- (rule (if ?variable ? ?value)
    (then ?goal ? ?goal-value))
    =>
    (retract ?f)
    (assert (variable ?goal ?goal-value)))

(defrule ask-question-no-legalvalues ""
    (declare (salience 10))
    (not (legalanswers $?))
    ?f1 <- (goal is ?variable)
    ?f2 <- (question ?variable ? ?text)
    =>
    (retract ?f1 ?f2)
    (format t "%s " ?text)
    (assert (variable ?variable (read))))

(defrule ask-question-legalvalues ""
    (declare (salience 10))
    (legalanswers ? $?answers)
    ?f1 <- (goal is ?variable)
    ?f2 <- (question ?variable ? ?text)
    =>
    (retract ?f1)
    (format t "%s " ?text)
    (printout t ?answers " ")
    (bind ?reply (read))
    (if (member (lowcase ?reply) ?answers) then (assert (variable ?variable ?reply))(retract ?f2)
    else (assert (goal is ?variable))))

(deffacts question-base
    (goal is interview.status)      ;задается конечная цель
    (legalanswers are a b yes no)   ;устанавливаются допустимые значения ответа
    
    (rule (if experience is a)(then experience.years is about.a.year))
    (rule (if experience is b)(then experience.years is more.than.two))
    (question experience is "Какой у Вас опыт работы? a-около года, b-больше")
    
        (rule (if experience.years is about.a.year and types is b)(then know.theory is true))
        (rule (if experience.years is about.a.year and types is a)(then know.theory is false))
        (question types is "Какие парадигмы программирования существуют?" crlf "a-функциональное, декларативное, ооп, семантичекое" crlf "b-ооп, функциональное, процедурное, типо-ориентированное")



        (rule (if experience.years is more.than.two and english is yes)(then technical.english is speaking))
        (rule (if experience.years is more.than.two and english is no)(then technical.english is not.speaking))
        (question english is "Владеете ли Вы техническим английским? yes no")

            (rule (if technical.english is speaking and dsl is a)(then dsl.examples is correct))
            (rule (if technical.english is speaking and dsl is b)(then dsl.examples is not.correct))
            (question dsl is "Выберите примеры DSl: a-CSS, HTML, Markdown, XML" crlf "b-SQL, PHP, Perl, Idris")

                (rule (if dsl.examples is correct and tree is a)(then interview.status is repeat.algorithms.and.come.in.a.month))
                (rule (if dsl.examples is correct and tree is b)(then interview.status is start.working.from.now))
                (question tree is "Средняя оценка сложности операций вставки, удаления в черно-красном дереве?" crlf "a- O(nlogn), b- O(logn)")

                (rule (if dsl.examples is not.correct and singleton is a)(then interview.status is probationing.to.remember.java))
                (rule (if dsl.examples is not.correct and singleton is b)(then interview.status is are.you.from.linguistic.school))
                (question singleton is "Сколько неленивых реализаций singleton'а существует в Java?" crlf "a- 2, b- 3")


            (rule (if technical.english is not.speaking and study is yes)(then interview.status is you.are.to.study))
            (rule (if technical.english is not.speaking and study is no)(then interview.status is we.will.call.you.later))
            (question study is "Готовы ли Вы к дополнительному обучению? yes no")

    





        
    (answer is "The interview is " interview.progress))

;(watch all)
;(agenda)
